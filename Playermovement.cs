﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Playermovement : MonoBehaviour
{
    public float speed;
    public GameObject mainCamera;
    public GameObject goal;

    public Text countText;
    public Text winText;
    public Text hintText;

    public AudioClip clearance;
    public AudioClip collect;
    public AudioSource audio;

    private Rigidbody rb;
    private int count;
    private int finish;

    void Start()
    {
        rb = GetComponent<Rigidbody>();

        count = 0;
        finish = 0;
        SetCountText();
        winText.text = "";
        hintText.text = "Hint : You have to find 3 crystal!";

        goal.SetActive(false);
    }

    void FixedUpdate()
    {
        if(finish == 0)
        {
            float moveHorizontal = Input.GetAxis("Horizontal");
            float moveVertical = Input.GetAxis("Vertical");

            float play_rotion = 0;//暫存方向

            if ((moveVertical != 0) || (moveHorizontal != 0)) play_rotion = mainCamera.transform.eulerAngles.y + Mathf.Atan2(moveHorizontal, moveVertical) * Mathf.Rad2Deg;
            else if (moveVertical > 0) play_rotion = mainCamera.transform.eulerAngles.y;
            else if (moveVertical < 0) play_rotion = mainCamera.transform.eulerAngles.y + 180;
            else if (moveHorizontal > 0) play_rotion = mainCamera.transform.eulerAngles.y + 90;
            else if (moveHorizontal < 0) play_rotion = mainCamera.transform.eulerAngles.y + 270;

            if ((moveVertical != 0) || (moveHorizontal != 0))//當有按方向鍵時
            {
                transform.eulerAngles = new Vector3(0, play_rotion, 0);//設定方向
                transform.position = transform.position + transform.forward * speed;//座標移動

                if (audio.isPlaying == false)
                {
                    audio.Play();
                }
            }
            else audio.Stop();
        }
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Treasure"))
        {
            other.gameObject.SetActive(false);
            
            AudioSource.PlayClipAtPoint(collect, transform.localPosition);

            count = count + 1;
            SetCountText();
        }

        if(other.gameObject.CompareTag("Goal"))
        {
            if(count >= 3)
            {
                finish = 1;
                SetCountText();
                
                AudioSource source = GameObject.FindGameObjectWithTag("clearance").GetComponent<AudioSource>();
                source.Play();
            }
        }
    }

    void SetCountText()
    {
        countText.text = "X " + count.ToString();
        int remaining = 3 - count;
        hintText.text = "Hint : You have to find " + remaining.ToString() + " crystal!";
        if (count >= 3)
        {
            hintText.text = "Hint : There is a shining point...";

            goal.SetActive(true);
        }

        if(finish == 1)
        {
            hintText.text = "";
            winText.text = "You Win!";

            AudioSource source = GameObject.FindGameObjectWithTag("BackgroundMusic").GetComponent<AudioSource>();
            source.Stop();
        }
    }
}
