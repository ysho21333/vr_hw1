﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject player;
    public float distance;

    private Vector3 offset;
    private float x, y;
    private float sensitivity = 1.0f;
    private Quaternion rotationEuler;
    private float minDistance = 3.0f;
    private float maxDistance = 7.0f;
    

    void Start()
    {
        offset = transform.position - player.transform.position;
    }
    
    void LateUpdate()
    {
        transform.position = player.transform.position + offset;

        x += Input.GetAxis("Mouse X") * sensitivity;
        y -= Input.GetAxis("Mouse Y") * sensitivity;

        // 鏡頭上下左右轉動角度限制
        if (x > 360) x -= 360;
        else if (x < 0) x += 360;

        if (y > 85) y = 85;
        else if (y < 5) y = 5;

        // 滑鼠滾輪控制鏡頭遠近
        distance -= Input.GetAxis("Mouse ScrollWheel") * sensitivity;
        distance = Mathf.Clamp(distance, minDistance, maxDistance);

        // 調整滑鼠移動後的相機位置
        rotationEuler = Quaternion.Euler(y, x, 0);
        transform.position = rotationEuler * new Vector3(0, 0, -distance) + player.transform.position;
        transform.rotation = rotationEuler;
        transform.LookAt(player.transform.position);
    }
}
