﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Treaure : MonoBehaviour
{
    private float radian = 0;
    private float perRadian = 0.05f;
    private float radius = 0.1f;
    private Vector3 oldPos;

    // Start is called before the first frame update
    void Start()
    {
        oldPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        radian += perRadian;
        float dy = Mathf.Cos(radian) * radius;
        transform.position = oldPos + new Vector3(0, dy, 0);
        transform.Rotate(new Vector3(0, 15, 0) * Time.deltaTime);
    }
}
